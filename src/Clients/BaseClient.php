<?php

namespace Sportmonks\FootballApi\Clients;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\TransferStats;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

class BaseClient
{
    protected Client $client;
    protected ?string $apiToken = NULL;
    private ?string $include = NULL;
    private ?string $select = NULL;
    private ?string $filters = NULL;
    private ?int $page = NULL;
    private ?int $pageSize = NULL;

    public function __construct()
    {
        $token = config('sportmonks.token');

        if (empty($token)) throw new InvalidArgumentException('No API token set');

        $this->apiToken = $token;
    }

    public function include(string|array $include = []): self
    {
        if (is_array($include)) $include = join(';', $include);
        $this->include = $include;
        return $this;
    }

    public function select(string|array $fields = []): self
    {
        if (is_array($fields)) $fields = join(',', $fields);
        $this->select = $fields;
        return $this;
    }

    public function filters(string|array $filters = []): self
    {
        if (is_array($filters)) $filters = join(';', $filters);
        $this->filters = $filters;
        return $this;
    }

    public function page(int $page): self
    {
        $this->page = $page;
        return $this;
    }

    public function pageSize(int $pageSize): self
    {
        $this->pageSize = $pageSize;
        return $this;
    }

    /**
     * @throws GuzzleException
     */
    protected function call(string $url, array $query = []): ?object
    {
        if (!empty($this->include)) $query['include'] = $this->include;
        if (!empty($this->select)) $query['select'] = $this->select;
        if (!empty($this->filters)) $query['filters'] = $this->filters;
        if (!empty($this->page)) $query['page'] = $this->page;
        $query['per_page'] = empty($this->pageSize)
            ? config('sportmonks.page_size')
            : $this->pageSize;

        $response = $this->client->get($url, [
            'headers' => [
                'Authorization' => $this->apiToken,
            ],
            'query' => [
                'tz' => config('sportmonks.timezone'),
                ...$query,
            ],
            'on_stats' => function (TransferStats $stats) use (&$url) {
                $url = $stats->getEffectiveUri();
            },
        ]);

        $code = $response->getStatusCode();

        if ($code === Response::HTTP_OK) {
            $response = json_decode($response->getBody()->getContents());
            $response->url = $url;
            return $response;
        }

        // @TODO handle error
        return NULL;
    }
}
