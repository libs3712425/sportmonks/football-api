<?php

namespace Sportmonks\FootballApi\Endpoints;

use GuzzleHttp\Exception\GuzzleException;
use Sportmonks\FootballApi\Clients\FootballClient;

/** @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/commentaries */
class Commentaries extends FootballClient
{
    private string $url = 'commentaries';

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/commentaries/get-all-commentaries
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function all(array $query = []): object
    {
        return $this->call($this->url, $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/commentaries/get-commentaries-by-fixture-id
     * @param int $fixtureId the id of the fixture
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byFixtureId(int $fixtureId, array $query = []): object
    {
        return $this->call("$this->url/fixtures/$fixtureId", $query);
    }
}
