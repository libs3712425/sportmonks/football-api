<?php

namespace Sportmonks\FootballApi\Endpoints;

use GuzzleHttp\Exception\GuzzleException;
use Sportmonks\FootballApi\Clients\CoreClient;

/** @link https://docs.sportmonks.com/football/v/core-api/endpoints/continents */
class Continents extends CoreClient
{
    private string $url = 'continents';

    /**
     * @link https://docs.sportmonks.com/football/v/core-api/endpoints/continents/get-all-continents
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function all(array $query = []): object
    {
        return $this->call($this->url, $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/v/core-api/endpoints/continents/get-continent-by-id
     * @param int $id the id of the continent
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byId(int $id, array $query = []): object
    {
        return $this->call("$this->url/$id", $query);
    }
}
