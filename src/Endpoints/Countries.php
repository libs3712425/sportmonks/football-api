<?php

namespace Sportmonks\FootballApi\Endpoints;

use GuzzleHttp\Exception\GuzzleException;
use Sportmonks\FootballApi\Clients\CoreClient;

/** @link https://docs.sportmonks.com/football/v/core-api/endpoints/countries */
class Countries extends CoreClient
{
    private string $url = 'countries';

    /**
     * @link https://docs.sportmonks.com/football/v/core-api/endpoints/countries/get-all-countries
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function all(array $query = []): object
    {
        return $this->call($this->url, $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/v/core-api/endpoints/countries/get-country-by-id
     * @param int $id the id of the country
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byId(int $id, array $query = []): object
    {
        return $this->call("$this->url/$id", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/v/core-api/endpoints/countries/get-countries-by-search
     * @param string $name the name to search
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function search(string $name, array $query = []): object
    {
        return $this->call("$this->url/search/$name", $query);
    }
}
