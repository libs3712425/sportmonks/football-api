<?php

namespace Sportmonks\FootballApi\Endpoints;

use GuzzleHttp\Exception\GuzzleException;
use Sportmonks\FootballApi\Clients\MySubscriptionClient;

/** @link https://docs.sportmonks.com/football/v/core-api/endpoints/filters */
class Filters extends MySubscriptionClient
{
    private string $url = 'filters';

    /**
     * @link https://docs.sportmonks.com/football/v/core-api/endpoints/filters/get-all-entity-filters
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function all(array $query = []): object
    {
        return $this->call("$this->url/entity", $query);
    }
}
