<?php

namespace Sportmonks\FootballApi\Endpoints;

use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use Sportmonks\FootballApi\Clients\FootballClient;

/** @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/fixtures */
class Fixtures extends FootballClient
{
    private string $url = 'fixtures';

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/fixtures/get-all-fixtures
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function all(array $query = []): object
    {
        return $this->call($this->url, $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/fixtures/get-fixture-by-id
     * @param int $id the id of the fixture
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byId(int $id, array $query = []): object
    {
        return $this->call("$this->url/$id", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/fixtures/get-fixtures-by-multiple-ids
     * @param string|array $ids the fixture ids
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byIds(string|array $ids, array $query = []): object
    {
        if (is_array($ids)) $ids = join(',', $ids);

        return $this->call("$this->url/multi/$ids", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/fixtures/get-fixtures-by-date
     * @param string|Carbon $date the date of the fixture
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byDate(string|Carbon $date, array $query = []): object
    {
        $date = Carbon::make($date)->toDateString();

        return $this->call("$this->url/date/$date", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/fixtures/get-fixtures-by-date-range
     * @param string|Carbon $startDate the starting date
     * @param string|Carbon $endDate the ending date
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byDateRange(string|Carbon $startDate, string|Carbon $endDate, array $query = []): object
    {
        $startDate = Carbon::make($startDate)->toDateString();
        $endDate = Carbon::make($endDate)->toDateString();

        return $this->call("$this->url/between/$startDate/$endDate", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/fixtures/get-fixtures-by-date-range-for-team
     * @param string|Carbon $startDate the starting date
     * @param string|Carbon $endDate the ending date
     * @param int $teamId the id of the team
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byDateRangeForTeam(string|Carbon $startDate, string|Carbon $endDate, int $teamId, array $query = []): object
    {
        $startDate = Carbon::make($startDate)->toDateString();
        $endDate = Carbon::make($endDate)->toDateString();

        return $this->call("$this->url/between/$startDate/$endDate/$teamId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/fixtures/get-fixtures-by-head-to-head
     * @param int $firstTeamId the id of the first team
     * @param int $secondTeamId the id of the second team
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function headToHead(int $firstTeamId, int $secondTeamId, array $query = []): object
    {
        return $this->call("$this->url/head-to-head/$firstTeamId/$secondTeamId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/fixtures/get-fixtures-by-search-by-name
     * @param string $name the name to search
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function search(string $name, array $query = []): object
    {
        return $this->call("$this->url/search/$name", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/fixtures/get-upcoming-fixtures-by-market-id
     * @param int $marketId the id of the market
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     * @link
     */
    public function upcomingByMarketId(int $marketId, array $query = []): object
    {
        return $this->call("$this->url/upcoming/markets/$marketId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/fixtures/get-upcoming-fixtures-by-tv-station-id
     * @param int $stationId the id of the tv station
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     * @link
     */
    public function upcomingByTvStationId(int $stationId, array $query = []): object
    {
        return $this->call("$this->url/upcoming/tv-stations/$stationId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/fixtures/get-past-fixtures-by-tv-station-id
     * @param int $stationId the id of the tv station
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     * @link
     */
    public function pastByTvStationId(int $stationId, array $query = []): object
    {
        return $this->call("$this->url/past/tv-stations/$stationId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/fixtures/get-latest-updated-fixtures
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function latest(array $query = []): object
    {
        return $this->call("$this->url/latest", $query);
    }
}
