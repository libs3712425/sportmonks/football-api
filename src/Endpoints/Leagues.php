<?php

namespace Sportmonks\FootballApi\Endpoints;

use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use Sportmonks\FootballApi\Clients\FootballClient;

/** @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/leagues */
class Leagues extends FootballClient
{
    private string $url = 'leagues';

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/leagues/get-all-leagues
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function all(array $query = []): object
    {
        return $this->call($this->url, $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/leagues/get-league-by-id
     * @param int $id the id of the league
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byId(int $id, array $query = []): object
    {
        return $this->call("$this->url/$id", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/leagues/get-leagues-by-live
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function live(array $query = []): object
    {
        return $this->call("$this->url/live", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/leagues/get-leagues-by-fixture-date
     * @param string|Carbon $date the date of the fixture
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byFixtureDate(string|Carbon $date, array $query = []): object
    {
        $date = Carbon::make($date)->toDateString();

        return $this->call("$this->url/date/$date", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/leagues/get-leagues-by-fixture-date
     * @param int $countryId the id of the country
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byCountryId(int $countryId, array $query = []): object
    {
        return $this->call("$this->url/countries/$countryId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/leagues/get-leagues-search-by-name
     * @param string $name the name to search
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function search(string $name, array $query = []): object
    {
        return $this->call("$this->url/search/$name", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/leagues/get-all-leagues-by-team-id
     * @param int $teamId the id of the team
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function allByTeamId(int $teamId, array $query = []): object
    {
        return $this->call("$this->url/teams/$teamId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/leagues/get-current-leagues-by-team-id
     * @param int $teamId the id of the team
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function currentByTeamId(int $teamId, array $query = []): object
    {
        return $this->call("$this->url/teams/$teamId/current", $query);
    }
}
