<?php

namespace Sportmonks\FootballApi\Endpoints;

use GuzzleHttp\Exception\GuzzleException;
use Sportmonks\FootballApi\Clients\FootballClient;

/** @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/livescores */
class Livescores extends FootballClient
{
    private string $url = 'livescores';

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/livescores/get-all-livescores
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function all(array $query = []): object
    {
        return $this->call($this->url, $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/livescores/get-inplay-livescores
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function inplay(array $query = []): object
    {
        return $this->call("$this->url/inplay", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/livescores/get-latest-updated-livescores
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function latest(array $query = []): object
    {
        return $this->call("$this->url/latest", $query);
    }
}
