<?php

namespace Sportmonks\FootballApi\Endpoints;

use GuzzleHttp\Exception\GuzzleException;
use Sportmonks\FootballApi\Clients\OddsClient;

/** @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/markets */
class Markets extends OddsClient
{
    private string $url = 'markets';

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/markets/get-all-markets
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function all(array $query = []): object
    {
        return $this->call($this->url, $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/markets/get-market-by-id
     * @param int $id the id of the market
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byId(int $id, array $query = []): object
    {
        return $this->call("$this->url/$id", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/markets/get-market-by-search
     * @param string $name the name to search
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function search(string $name, array $query = []): object
    {
        return $this->call("$this->url/search/$name", $query);
    }
}
