<?php

namespace Sportmonks\FootballApi\Endpoints;

use GuzzleHttp\Exception\GuzzleException;
use Sportmonks\FootballApi\Clients\FootballClient;

/** @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/news */
class News extends FootballClient
{
    private string $url = 'news';

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/news/get-pre-match-news
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function all(array $query = []): object
    {
        return $this->call("$this->url/pre-match", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/news/get-pre-match-news-by-season-id
     * @param int $seasonId the id of the season
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function bySeasonId(int $seasonId, array $query = []): object
    {
        return $this->call("$this->url/pre-match/seasons/$seasonId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/news/get-pre-match-news-for-upcoming-fixtures
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function upcoming(array $query = []): object
    {
        return $this->call("$this->url/pre-match/upcoming", $query);
    }
}
