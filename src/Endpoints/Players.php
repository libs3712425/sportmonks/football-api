<?php

namespace Sportmonks\FootballApi\Endpoints;

use GuzzleHttp\Exception\GuzzleException;
use Sportmonks\FootballApi\Clients\FootballClient;

/** @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/players */
class Players extends FootballClient
{
    private string $url = 'players';

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/players/get-all-players
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function all(array $query = []): object
    {
        return $this->call($this->url, $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/players/get-player-by-id
     * @param int $id the id of the player
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byId(int $id, array $query = []): object
    {
        return $this->call("$this->url/$id", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/players/get-players-by-country-id
     * @param int $countryId the id of the country
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byCountryId(int $countryId, array $query = []): object
    {
        return $this->call("$this->url/countries/$countryId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/players/get-players-by-search-by-name
     * @param string $name the name to search
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function search(string $name, array $query = []): object
    {
        return $this->call("$this->url/search/$name", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/players/get-last-updated-players
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function latest(array $query = []): object
    {
        return $this->call("$this->url/latest", $query);
    }
}
