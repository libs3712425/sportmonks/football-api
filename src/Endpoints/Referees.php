<?php

namespace Sportmonks\FootballApi\Endpoints;

use GuzzleHttp\Exception\GuzzleException;
use Sportmonks\FootballApi\Clients\FootballClient;

/** @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/referees */
class Referees extends FootballClient
{
    private string $url = 'referees';

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/referees/get-all-referees
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function all(array $query = []): object
    {
        return $this->call($this->url, $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/referees/get-referee-by-id
     * @param int $id the id of the referee
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byId(int $id, array $query = []): object
    {
        return $this->call("$this->url/$id", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/referees/get-referees-by-country-id
     * @param int $countryId the id of the country
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byCountryId(int $countryId, array $query = []): object
    {
        return $this->call("$this->url/countries/$countryId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/referees/get-referees-by-season-id
     * @param int $seasonId the id of the season
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function bySeasonId(int $seasonId, array $query = []): object
    {
        return $this->call("$this->url/seasons/$seasonId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/referees/get-referees-search-by-name
     * @param string $name the name to search
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function search(string $name, array $query = []): object
    {
        return $this->call("$this->url/search/$name", $query);
    }
}
