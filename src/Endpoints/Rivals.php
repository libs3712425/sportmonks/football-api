<?php

namespace Sportmonks\FootballApi\Endpoints;

use GuzzleHttp\Exception\GuzzleException;
use Sportmonks\FootballApi\Clients\FootballClient;

/** @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/rivals */
class Rivals extends FootballClient
{
    private string $url = 'rivals';

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/rivals/get-all-rivals
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function all(array $query = []): object
    {
        return $this->call($this->url, $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/rivals/get-rivals-by-team-id
     * @param int $teamId the id of the team
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byTeamId(int $teamId, array $query = []): object
    {
        return $this->call("$this->url/teams/rivals/$teamId", $query);
    }
}
