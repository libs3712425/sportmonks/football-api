<?php

namespace Sportmonks\FootballApi\Endpoints;

use GuzzleHttp\Exception\GuzzleException;
use Sportmonks\FootballApi\Clients\CoreClient;

/** @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/schedules */
class Schedules extends CoreClient
{
    private string $url = 'schedules';

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/schedules/get-schedules-by-season-id
     * @param int $seasonId the season id
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function bySeasonId(int $seasonId, array $query = []): object
    {
        return $this->call("$this->url/seasons/$seasonId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/schedules/get-schedules-by-team-id
     * @param int $teamId the team id
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byTeamId(int $teamId, array $query = []): object
    {
        return $this->call("$this->url/teams/$teamId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/schedules/get-schedules-by-season-id-and-team-id
     * @param int $seasonId the season id
     * @param int $teamId the team id
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function bySeasonIdAndTeamId(int $seasonId, int $teamId, array $query = []): object
    {
        return $this->call("$this->url/seasons/$seasonId/teams/$teamId", $query);
    }
}
