<?php

namespace Sportmonks\FootballApi\Endpoints;

use GuzzleHttp\Exception\GuzzleException;
use Sportmonks\FootballApi\Clients\FootballClient;

/** @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/team-squads */
class Squads extends FootballClient
{
    private string $url = 'squads';

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/team-squads/get-team-squad-by-team-id
     * @param int $teamId the id of the team
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byTeamId(int $teamId, array $query = []): object
    {
        return $this->call("$this->url/teams/$teamId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/team-squads/get-team-squad-by-team-and-season-id
     * @param int $teamId the id of the team
     * @param int $seasonId the id of the season
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byTeamAndSeasonId(int $teamId, int $seasonId, array $query = []): object
    {
        return $this->call("$this->url/seasons/$seasonId/teams/$teamId", $query);
    }
}
