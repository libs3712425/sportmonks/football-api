<?php

namespace Sportmonks\FootballApi\Endpoints;

use GuzzleHttp\Exception\GuzzleException;
use Sportmonks\FootballApi\Clients\FootballClient;

/** @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/standings */
class Standings extends FootballClient
{
    private string $url = 'standings';

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/standings/get-all-standings
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function all(array $query = []): object
    {
        return $this->call($this->url, $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/standings/get-standings-by-season-id
     * @param int $seasonId the id of the season
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function bySeasonId(int $seasonId, array $query = []): object
    {
        return $this->call("$this->url/seasons/$seasonId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/standings/get-standing-correction-by-season-id
     * @param int $seasonId the id of the season
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function correctionBySeasonId(int $seasonId, array $query = []): object
    {
        return $this->call("$this->url/corrections/seasons/$seasonId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/standings/get-standings-by-round-id
     * @param int $roundId the id of the round
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byRoundId(int $roundId, array $query = []): object
    {
        return $this->call("$this->url/rounds/$roundId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/standings/get-live-standings-by-league-id
     * @param int $leagueId the id of the league
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function liveByLeagueId(int $leagueId, array $query = []): object
    {
        return $this->call("$this->url/live/leagues/$leagueId", $query);
    }
}
