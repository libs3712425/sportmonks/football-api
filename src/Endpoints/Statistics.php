<?php

namespace Sportmonks\FootballApi\Endpoints;

use GuzzleHttp\Exception\GuzzleException;
use Sportmonks\FootballApi\Clients\CoreClient;

/** @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/statistics */
class Statistics extends CoreClient
{
    private string $url = 'statistics';

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/statistics/get-season-statistics-by-participant
     * @param int $seasonId the id of the season
     * @param int $participantId the id of the player, team, coach or referee
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function bySeasonForParticipant(int $seasonId, int $participantId, array $query = []): object
    {
        return $this->call("$this->url/seasons/$participantId/$seasonId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/statistics/get-stage-statistics-by-id
     * @param int $stageId the id of the stage
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byStageId(int $stageId, array $query = []): object
    {
        return $this->call("$this->url/stages/$stageId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/statistics/get-round-statistics-by-id
     * @param int $roundId the id of the round
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byRoundId(int $roundId, array $query = []): object
    {
        return $this->call("$this->url/rounds/$roundId", $query);
    }
}
