<?php

namespace Sportmonks\FootballApi\Endpoints;

use GuzzleHttp\Exception\GuzzleException;
use Sportmonks\FootballApi\Clients\FootballClient;

/** @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/topscorers */
class Topscorers extends FootballClient
{
    private string $url = 'topscorers';

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/topscorers/get-topscorers-by-season-id
     * @param int $seasonId the id of the season
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function bySeasonId(int $seasonId, array $query = []): object
    {
        return $this->call("$this->url/seasons/$seasonId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/topscorers/get-topscorers-by-stage-id
     * @param int $stageId the id of the stage
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byStageId(int $stageId, array $query = []): object
    {
        return $this->call("$this->url/stages/$stageId", $query);
    }
}
