<?php

namespace Sportmonks\FootballApi\Endpoints;

use Carbon\Carbon;
use GuzzleHttp\Exception\GuzzleException;
use Sportmonks\FootballApi\Clients\FootballClient;

/** @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/transfers */
class Transfers extends FootballClient
{
    private string $url = 'transfers';

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/transfers/get-all-transfers
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function all(array $query = []): object
    {
        return $this->call($this->url, $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/transfers/get-transfer-by-id
     * @param int $id the id of the transfer
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byId(int $id, array $query = []): object
    {
        return $this->call("$this->url/$id", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/transfers/get-latest-transfers
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function latest(array $query = []): object
    {
        return $this->call("$this->url/latest", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/transfers/get-transfers-between-date-range
     * @param string|Carbon $startDate the starting date
     * @param string|Carbon $endDate the ending date
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byDateRange(string|Carbon $startDate, string|Carbon $endDate, array $query = []): object
    {
        $startDate = Carbon::make($startDate)->toDateString();
        $endDate = Carbon::make($endDate)->toDateString();

        return $this->call("$this->url/between/$startDate/$endDate", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/transfers/get-transfers-by-team-id
     * @param int $teamId the id of the team
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byTeamId(int $teamId, array $query = []): object
    {
        return $this->call("$this->url/teams/$teamId", $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/transfers/get-transfers-by-player-id
     * @param int $playerId the id of the player
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byPlayerId(int $playerId, array $query = []): object
    {
        return $this->call("$this->url/players/$playerId", $query);
    }
}
