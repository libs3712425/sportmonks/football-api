<?php

namespace Sportmonks\FootballApi\Endpoints;

use GuzzleHttp\Exception\GuzzleException;
use Sportmonks\FootballApi\Clients\CoreClient;

/** @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/types */
class Types extends CoreClient
{
    private string $url = 'types';

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/types/get-all-types
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function all(array $query = []): object
    {
        return $this->call($this->url, $query);
    }

    /**
     * @link https://docs.sportmonks.com/football/endpoints-and-entities/endpoints/types/get-type-by-id
     * @param int $id the id of the type
     * @param array $query the query params
     * @return object
     * @throws GuzzleException
     */
    public function byId(int $id, array $query = []): object
    {
        return $this->call("$this->url/$id", $query);
    }
}
