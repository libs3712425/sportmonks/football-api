<?php

namespace Sportmonks\FootballApi;

use Sportmonks\FootballApi\Endpoints\Bookmakers;
use Sportmonks\FootballApi\Endpoints\Coaches;
use Sportmonks\FootballApi\Endpoints\Commentaries;
use Sportmonks\FootballApi\Endpoints\Fixtures;
use Sportmonks\FootballApi\Endpoints\Leagues;
use Sportmonks\FootballApi\Endpoints\Livescores;
use Sportmonks\FootballApi\Endpoints\Markets;
use Sportmonks\FootballApi\Endpoints\News;
use Sportmonks\FootballApi\Endpoints\Players;
use Sportmonks\FootballApi\Endpoints\Referees;
use Sportmonks\FootballApi\Endpoints\Rivals;
use Sportmonks\FootballApi\Endpoints\Rounds;
use Sportmonks\FootballApi\Endpoints\Schedules;
use Sportmonks\FootballApi\Endpoints\Seasons;
use Sportmonks\FootballApi\Endpoints\Squads;
use Sportmonks\FootballApi\Endpoints\Stages;
use Sportmonks\FootballApi\Endpoints\Standings;
use Sportmonks\FootballApi\Endpoints\States;
use Sportmonks\FootballApi\Endpoints\Statistics;
use Sportmonks\FootballApi\Endpoints\Teams;
use Sportmonks\FootballApi\Endpoints\Topscorers;
use Sportmonks\FootballApi\Endpoints\Transfers;
use Sportmonks\FootballApi\Endpoints\TvStations;
use Sportmonks\FootballApi\Endpoints\Types;
use Sportmonks\FootballApi\Endpoints\Venues;

/**
 * @link https://docs.sportmonks.com/football/welcome/getting-started
 * TODO: Predictions, Pre-Match Odds, Inplay Odds
 */
class FootballApi
{
    public static function bookmakers(): Bookmakers
    {
        return new Bookmakers();
    }

    public static function coaches(): Coaches
    {
        return new Coaches();
    }

    public static function commentaries(): Commentaries
    {
        return new Commentaries();
    }

    public static function fixtures(): Fixtures
    {
        return new Fixtures();
    }

    public static function leagues(): Leagues
    {
        return new Leagues();
    }

    public static function livescores(): Livescores
    {
        return new Livescores();
    }

    public static function markets(): Markets
    {
        return new Markets();
    }

    public static function news(): News
    {
        return new News();
    }

    public static function players(): Players
    {
        return new Players();
    }

    public static function referees(): Referees
    {
        return new Referees();
    }

    public static function rivals(): Rivals
    {
        return new Rivals();
    }

    public static function rounds(): Rounds
    {
        return new Rounds();
    }

    public static function schedules(): Schedules
    {
        return new Schedules();
    }

    public static function seasons(): Seasons
    {
        return new Seasons();
    }

    public static function squads(): Squads
    {
        return new Squads();
    }

    public static function stages(): Stages
    {
        return new Stages();
    }

    public static function standings(): Standings
    {
        return new Standings();
    }

    public static function states(): States
    {
        return new States();
    }

    public static function statistics(): Statistics
    {
        return new Statistics();
    }

    public static function teams(): Teams
    {
        return new Teams();
    }

    public static function topScorers(): Topscorers
    {
        return new Topscorers();
    }

    public static function transfers(): Transfers
    {
        return new Transfers();
    }

    public static function tvStations(): TvStations
    {
        return new TvStations();
    }

    public static function types(): Types
    {
        return new Types();
    }

    public static function venues(): Venues
    {
        return new Venues();
    }
}
