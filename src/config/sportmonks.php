<?php

if (!function_exists('config_path')) {
    function config_path (string $path = '') : string
    {
        return app()->basePath() . '/config' . ($path ? "/$path" : $path);
    }
}

return [
    'token' => env('SPORTMONKS_TOKEN'),
    'timezone' => env('SPORTMONKS_TIMEZONE') ?? env('APP_TIMEZONE'),
    'page_size' => env('SPORTMONKS_PAGE_SIZE') ?? 25,
];
