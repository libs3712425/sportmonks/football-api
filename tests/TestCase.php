<?php

use Sportmonks\FootballApi\Clients\FootballClient;
use Sportmonks\FootballApi\FootballApiServiceProvider;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Testing\TestCase as BaseTest;
use Illuminate\Support\Facades\Config;

class TestCase extends BaseTest
{
    public function createApplication(): Application
    {
        $app = require __DIR__ . '/../vendor/laravel/laravel/bootstrap/app.php';
        $app->register(FootballApiServiceProvider::class);
        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    /**
     * @test
     */
    public function it_throws_an_exception_if_no_api_token_set()
    {
        $this->expectException(InvalidArgumentException::class);

        Config::set('sportmonks.token', '');
        new FootballClient();
    }

    protected function setUp(): void
    {
        parent::setUp();
        Config::set('sportmonks.token', 'YOUR_TOKEN_HERE');
    }
}
